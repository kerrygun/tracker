<?php

namespace AppBundle\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;


class StdJson extends ArrJson
{
    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return empty($value)
            ? null
            : json_encode($value, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
        ;
    }

	/**
	 * {@inheritdoc}
	 */
	public function convertToPHPValue($value, AbstractPlatform $platform)
	{
		return (empty($value))
			? null
			: json_decode((is_resource($value)) ? stream_get_contents($value) : $value)
		;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName()
	{
		return 'std_json';
	}
}
