<?php

namespace AppBundle\Monolog\Processor;

use AppBundle\Service\ProcessService;


class UidProcessor
{
    /**
     * @var ProcessService
     */
    protected $processService;

    /**
     * @param ProcessService $processService
     */
    public function __construct(
        ProcessService $processService
    ) {
        $this->processService = $processService;
    }


    /**
     * @inheritdoc
     */
    public function __invoke(array $record)
    {
        $record['extra']['uid'] = $this->processService->getId();

        return $record;
    }
}
