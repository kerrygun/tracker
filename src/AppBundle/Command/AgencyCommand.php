<?php

namespace AppBundle\Command;

use AppBundle\Service\ClickService;
use AppBundle\Dto\ResponseAgency;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;


class AgencyCommand extends Command
{
    /**
     * @var ClickService
     */
    protected $clickService;

    /**
     * @param ClickService    $clickService
     */
    public function __construct(
        ClickService    $clickService
    ) {
        parent::__construct('Get Agency');

        $this->clickService    = $clickService;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('cookie:agency')
            ->setDescription(sprintf('Cookie agency'))
            ->addArgument('cookie',InputArgument::REQUIRED, 'cookie')
            ->addArgument('date',  InputArgument::OPTIONAL, 'date')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cookie = $input->getArgument('cookie');
        $click  = $this->clickService->getLastPaidClick($cookie, null);
        $agency = $click->getAgency();

        $output->write(json_encode(new ResponseAgency($agency, new DateTime($input->getArgument('date')))));
    }
}
