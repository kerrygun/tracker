<?php

namespace AppBundle\Command;

use AppBundle\Entity\Postback;
use AppBundle\Service\Array2XlsxService;
use AppBundle\Service\ReportPostbackMailerService;
use AppBundle\Service\ReportPostbackService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;


class ReportPostbackCommand extends Command
{
    /**
     * @var ReportPostbackService
     */
    protected $reportService;

    /**
     * @var Array2XlsxService
     */
    protected $array2XlsxService;

    /**
     * @var ReportPostbackMailerService
     */
    protected $reportMailer;

    /**
     * @param ReportPostbackService       $reportService
     * @param Array2XlsxService           $array2XlsxService
     * @param ReportPostbackMailerService $reportMailer
     */
    public function __construct(
        ReportPostbackService       $reportService,
        Array2XlsxService           $array2XlsxService,
        ReportPostbackMailerService $reportMailer
    ) {
        parent::__construct('ReportPostback');

        $this->reportService     = $reportService;
        $this->array2XlsxService = $array2XlsxService;
        $this->reportMailer      = $reportMailer;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('report:postback')
            ->setDescription(sprintf('Отчёт по постбэкам'))

            ->addArgument('from', InputArgument::OPTIONAL, 'Date from YYYY-MM-DD')
            ->addArgument('till', InputArgument::OPTIONAL, 'Date till YYYY-MM-DD')

            ->addOption('mail', 'm', InputOption::VALUE_NONE, 'mail report and delete file')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $from = new DateTime($input->getArgument('from') ?? date('Y-m-d', strtotime('first day of last month')));
        $till = new DateTime($input->getArgument('till') ?? date('Y-m-d', strtotime('first day of this month')));

        assert($from <= $till);

        $array = array_map(function(Postback $postback) {
            $parameters = $postback->getClick()->getParamsMap();
            return [
                $postback->getCreatedAt()->format('d.m.Y'),
                $postback->getCreatedAt()->format('H:i'),
                $postback->getApplicationId(),
                $postback->getClick()->getClickId(),
                $parameters['utm_source'] ?? null,
                $parameters['utm_medium'] ?? null,
                $parameters['utm_campaign'] ?? null,
            ];
        }, $this->reportService->execute($from, $till));

        array_unshift($array, ['Дата', 'Время', 'Заявка', 'Click ID', 'utm_source', 'utm_medium', 'utm_campaign']);

        $path = sprintf('Postback_report_%s-%s.xlsx', $from->format('Y.m.d'), $till->format('Y.m.d'));

        $this->array2XlsxService->save($array, $path);

        if ($input->getOption('mail'))
        {
            $this->reportMailer->send($path);

            unlink($path);
        }
    }
}
