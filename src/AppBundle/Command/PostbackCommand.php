<?php

namespace AppBundle\Command;

use AppBundle\Exception\ActionException;
use AppBundle\Service\ClickService;
use AppBundle\Service\PostbackService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class PostbackCommand extends Command
{
    /**
     * @var int
     */
    protected $clickSearchDays;
    /**
     * @var ClickService
     */
    protected $clickService;

    /**
     * @var PostbackService
     */
    protected $postbackService;

    /**
     * @param int             $clickSearchDays
     * @param ClickService    $clickService
     * @param PostbackService $postbackService
     */
    public function __construct(
        int             $clickSearchDays,
        ClickService    $clickService,
        PostbackService $postbackService
    ) {
        parent::__construct('TestPostback');

        $this->clickService    = $clickSearchDays;
        $this->clickService    = $clickService;
        $this->postbackService = $postbackService;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('test:postback')
            ->setDescription(sprintf('Отчёт по постбэкам'))

            ->addArgument('cookie',      InputArgument::REQUIRED, 'cookie')
            ->addArgument('action',      InputArgument::REQUIRED, 'action')
            ->addArgument('application', InputArgument::REQUIRED, 'application')
            ->addArgument('till',        InputArgument::REQUIRED, 'till')
            ->addArgument('status',      InputArgument::REQUIRED, 'status')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cookieId = $input->getArgument('cookie');

        $till = $input->getArgument('till') ? (new DateTime())->setTimestamp($input->getArgument('till')) : null;

        $output->write($till);

        if (! $click = $this->clickService->getLastPaidClick($cookieId, $till)) {
            $output->write(sprintf('Не найден отслеживающий cookie "%s"', $cookieId));
        }

        $output->write(sprintf('Найден отслеживающий cookie "%s"', $cookieId));

        try {
            $this->postbackService->makePostback($click, $input->getArguments());
        } catch (ActionException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), null, 400);
        }
    }
}
