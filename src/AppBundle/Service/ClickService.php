<?php

namespace AppBundle\Service;

use AppBundle\Entity\Agency;
use AppBundle\Entity\Click;
use AppBundle\Entity\Parameter;
use AppBundle\Entity\Target;
use AppBundle\Repository\QueryClicks;
use AppBundle\Repository\QueryLastClick;
use Doctrine\ORM\EntityManagerInterface;
use DateTime;


class ClickService
{
    /**
     * @var int
     */
    protected $clickSearchDays;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var TargetService
     */
    protected $targetService;

    /**
     * @var QueryService
     */
    protected $queryService;

    /**
     * @param int                    $clickSearchDays
     * @param EntityManagerInterface $entityManager
     * @param TargetService          $targetService
     * @param QueryService           $queryService
     */
    public function __construct(
        int                    $clickSearchDays,
        EntityManagerInterface $entityManager,
        TargetService          $targetService,
        QueryService           $queryService
    )
    {
        $this->clickSearchDays = $clickSearchDays;
        $this->entityManager   = $entityManager;
        $this->targetService   = $targetService;
        $this->queryService    = $queryService;
    }


    /**
     * @param string $cookieId
     * @param Agency $agency
     * @param array  $params
     * @param string $userAgent
     * @param string $referrer
     *
     * @return Click
     */
    public function createClick(string $cookieId, ?Agency $agency, array $params, ?string $userAgent, ?string $referrer)
    {
        $targetKey  = ($agency and $agency->getTargetKey()) ? $agency->getTargetKey() : Target::DEFAULT_KEY;
        $targetName = ! empty($params[$targetKey]) ? $params[$targetKey] : null;

        $target = $this->targetService->getTarget($targetName);

        $click = (new Click($cookieId))
            ->setTarget   ($target)
            ->setUserAgent($userAgent)
            ->setReferrer ($referrer);

        if ($agency) {
            $click->setAgency($agency);

            if ($idKey = $agency->getIdKey() and array_key_exists($idKey, $params)) {
                $click->setClickId($params[$idKey]);
            }
        }

        $this->entityManager->persist($click);

        foreach ($params as $name => $value)
        {
            $this->entityManager->persist($parameter = new Parameter($click, $name, $value));
            $click->addParameter($parameter);
        }

        $this->entityManager->flush();

        return $click;
    }

    /**
     * @param string   $cookieId
     * @param DateTime $till
     *
     * @return Click
     */
    public function getLastClick(string $cookieId, ?DateTime $till)
    {
        $params = array(
            'cookieId' => $cookieId
        );

        if ($till) {
            $params['till'] = $till;
            $params['from'] = $till->modify(sprintf('-%s days', $this->clickSearchDays));
        }

        return $this->queryService->execute(QueryLastClick::class, $params);
    }

    /**
     * @param string        $cookieId
     * @param DateTime|null $till
     *
     * @return Click
     */
    public function getLastPaidClick(string $cookieId, ?DateTime $till)
    {
        $params = array(
            'cookieId' => $cookieId,
            'paid'     => true
        );

        if ($till) {
            $params['till'] = $till;
            $params['from'] = (clone $till)->modify(sprintf('-%s days', $this->clickSearchDays));
        }

        /** @var Click[] $clicks */
        $clicks = $this->queryService->execute(QueryClicks::class, $params);

        if (count($clicks) === 1) {
            return current($clicks);
        }

        foreach ($clicks as $click) {
            if ($medium = $click->getParameter(Parameter::MEDIUM)
                and in_array($medium->getValue(), [Parameter::MEDIUM_ORGANIC, Parameter::MEDIUM_NONE])
            ) {
                return $click;
            }
        }

        return reset($clicks);
    }
}