<?php

namespace AppBundle\Service;

use GuzzleHttp\Client;


class PostbackClient
{
    /**
     * @var array
     */
    protected $httpConfig;

    /**
     * @param array $httpConfig
     */
    public function __construct(
        array $httpConfig
    ) {
        $this->httpConfig = $httpConfig;
    }

    /**
     * @param string $url
     */
    public function makePostback(string $url)
    {
        $client = new Client($this->httpConfig);
        $client->request('GET', $url);
    }
}
