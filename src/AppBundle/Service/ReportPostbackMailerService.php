<?php

namespace AppBundle\Service;

use Assert\Assert;
use Psr\Log\LoggerInterface;
use InvalidArgumentException;
use Swift_Mailer;
use Swift_Message;
use Swift_Attachment;


class ReportPostbackMailerService
{
    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected $fromName;

    /**
     * @var string
     */
    protected $fromEmail;

    /**
     * @var string[]
     */
    protected $emailTo;

    /**
     * @var string[]
     */
    protected $emailCc;

    /**
     * @var string[]
     */
    protected $emailBcc;

    /**
     * @param Swift_Mailer    $mailer
     * @param LoggerInterface $logger
     * @param string          $fromName
     * @param string          $fromEmail
     * @param string[]        $emailTo
     * @param string[]        $emailCc
     * @param string[]        $emailBcc
     */
    public function __construct(
        Swift_Mailer    $mailer,
        LoggerInterface $logger,
        string          $fromName,
        string          $fromEmail,
        array           $emailTo,
        array           $emailCc,
        array           $emailBcc
    ) {
        Assert::that($emailTo)->all()->email();
        Assert::that($emailCc)->all()->email();
        Assert::that($emailBcc)->all()->email();

        $this->mailer    = $mailer;
        $this->logger    = $logger;
        $this->fromName  = $fromName;
        $this->fromEmail = $fromEmail;
        $this->emailTo   = $emailTo;
        $this->emailCc   = $emailCc;
        $this->emailBcc  = $emailBcc;
    }

    /**
     * @param string $path
     *
     * @throws InvalidArgumentException
     */
    public function send(string $path)
    {
        $this->logger->info('Отправка документа отчета на Email');

        $sentCount = $this->mailer->send((new Swift_Message(
            '[SME.PostbackReport]',
            ''
        ))
            ->setFrom($this->fromEmail, $this->fromName)

            ->setTo($this->emailTo)
            ->setCc($this->emailCc)
            ->setBcc($this->emailBcc)

            ->attach(Swift_Attachment::fromPath($path))
        );

        $this->mailer->getTransport()
            ->stop()
        ;

        $recipients = array(
            'to'  => $this->emailTo,
            'cc'  => $this->emailCc,
            'bcc' => $this->emailBcc,
        );

        if ($sentCount) {
            $this->logger->info('Успешно отправлен документ скоринга на Email', $recipients);
        } else {
            $this->logger->error('Документ скоринга не был отправлен ни на один из адресов', $recipients);
        }

    }
}
