<?php

namespace AppBundle\Service;

use AppBundle\Entity\Postback;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\Criteria;
use Psr\Log\LoggerInterface;

use DateTime;


class ReportPostbackService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface        $logger
    )
    {
        $this->entityManager  = $entityManager;
        $this->logger         = $logger;
    }

    /**
     * @param DateTime $from
     * @param DateTime $till
     *
     * @return array
     */
    public function execute(DateTime $from, DateTime $till)
    {
        $builder = $this->entityManager->createQueryBuilder()
            ->select('p')
            ->from(Postback::class, 'p')

            ->addSelect('c')
            ->join('p.click', 'c')

            ->andWhere('p.createdAt >= :from')
            ->setParameter('from', $from)

            ->andWhere('p.createdAt <= :till')
            ->setParameter('till', $till)

            ->orderBy('p.createdAt', Criteria::ASC)
        ;

        return $builder->getQuery()->getResult();
    }
}
