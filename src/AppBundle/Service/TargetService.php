<?php

namespace AppBundle\Service;

use AppBundle\Entity\Target;
use AppBundle\Repository\QueryTarget;
use AppBundle\Exception\TargetException;


class TargetService
{
    /**
     * @var QueryService
     */
    protected $queryService;

    /**
     * @var UrlService
     */
    protected $urlService;

    /**
     * @param QueryService $queryService
     * @param UrlService   $urlService
     */
    public function __construct(
        QueryService $queryService,
        UrlService   $urlService
    )
    {
        $this->queryService = $queryService;
        $this->urlService   = $urlService;

    }


    /**
     * @param string $name
     *
     * @return Target
     */
    public function getTarget(?string $name)
    {
        $params = array();

        if ($name) {
            $params['name'] = $name;
        }

        if (! $target = $this->queryService->execute(QueryTarget::class, $params))
        {
            if (! $target = $this->queryService->execute(QueryTarget::class, [])) {
                throw new TargetException('Target not found', 404);
            }
        }

        return $target;
    }

    /**
     * @param Target $target
     * @param array  $parameters
     *
     * @return string
     */
    public function getUrl(Target $target, array $parameters)
    {
        return $this->urlService->enrichUrl($target->getUrl(), $parameters);
    }
}