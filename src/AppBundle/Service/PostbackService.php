<?php

namespace AppBundle\Service;

use AppBundle\Entity\Click;
use AppBundle\Entity\Postback;
use AppBundle\Exception\ActionException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use InvalidArgumentException;
use Exception;


class PostbackService
{
    /**
     * @var ActionService
     */
    protected $actionService;

    /**
     * @var UrlService
     */
    protected $urlService;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var PostbackClient
     */
    protected $postbackClient;

    /**
     * @var LoggerInterface
     */
    protected $logger;

	/**
	 * @var array<string, AgencyProcessor>
	 */
    protected $processors;


    public function __construct(
        array                  $processors,
        ActionService          $actionService,
        UrlService             $urlService,
        EntityManagerInterface $entityManager,
        PostbackClient         $postbackClient,
        LoggerInterface        $logger
    ) {
        $this->actionService  = $actionService;
        $this->urlService     = $urlService;
        $this->entityManager  = $entityManager;
        $this->postbackClient = $postbackClient;
        $this->logger         = $logger;

	    $this->processors = array(); array_walk($processors, function(AgencyProcessorInterface $processor)
        {
        	if (array_key_exists($processor->getSource(), $this->processors))
        		throw new InvalidArgumentException(sprintf('Уже есть такой процессор `%s`', $processor->getSource()));

	        $this->processors[$processor->getSource()] = $processor;
        });
    }


    /**
     * @param Click $click
     * @param array $params
     *
     * @return bool
     */
    public function makePostback(Click $click, array $params)
    {
    	$actionName    = trim($params['action']      ?? '');
    	$applicationId = trim($params['application'] ?? '');
    	$status        = trim($params['status']      ?? '');
    	$reason        = trim($params['reason']      ?? '');

        if (empty($actionName))
            throw new ActionException('Не задан параметр action');

        if (empty($applicationId))
            throw new ActionException('Не задан параметр application');

		if (! $action = $this->actionService->getAction($click->getTarget(), $actionName))
			throw new ActionException(sprintf('Не найдено Действие `%s`', $actionName));

		if ($agencyAction = $click->getAgency()->getPostbackAction() and $agencyAction->getId() !== $action->getId()) {
			// Кирилл не помнит для чего это!
		    return false;
        }

        $this->logger->info(sprintf('Найдено Действие `%s`', $action->getTitle() ?? $action->getName()));

        if (! $agency = $click->getAgency()) {
            $this->logger->info('Источник не определен');

            return false;
        }

	    if (! $url = $agency->getPostBackUrl()) {
		    $this->logger->info(sprintf('Нет postback адреса для источника "%s"', $agency->getName()));

		    return false;
	    }

        $allParams = array_merge($click->getParamsMap(), $params);

        if (array_key_exists($agency->getSource(), $this->processors)) {
            /** @var $processor AgencyProcessorInterface */
            $processor = $this->processors[$agency->getSource()];

            if (! $processor->isProcessable($status, $reason)) {
                $this->logger->info(sprintf('Не обрабатывается при статусе "%s:%s"', $status, $reason));

                return false;
            }

            $url = $this->urlService->replaceParameters($url, $processor->getParams($allParams));

            $this->logger->info(sprintf('Применён процессор `%s`', $processor->getSource()));
        } else {
            $url = $this->urlService->replaceParameters($url, $allParams);
        }

        $this->logger->info(sprintf('Сообщаем о Действии "%s" на "%s"', $action->getName(), $url));

        $this->entityManager->persist($postback = new Postback($click, $action, $applicationId, $url));

        try {
            $this->postbackClient->makePostback($url);

            $postback->setCompleted();
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        $this->entityManager->flush();

        return true;
    }
}
