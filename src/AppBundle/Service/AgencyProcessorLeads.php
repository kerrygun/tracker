<?php

namespace AppBundle\Service;


class AgencyProcessorLeads extends AgencyProcessor
{
    /**
     * @var string
     */
    protected $source = 'leads.su';

	/**
	 * @inheritdoc
	 */
	public function getParams(array $source): array
	{
	    $map = array(
	        'rejected' => ['REJECTED', 'CANCELED'],
            'approved' => ['SUCCESS'],
        );

	    if (array_key_exists('status', $source)) {
            foreach ($map as $name => $statuses) {
                if (in_array($source['status'], $statuses)) {
                    $source['status'] = $name;
                    break;
                }
            }
        }

		return $source;
	}
}
