<?php

namespace AppBundle\Service;


class ProcessService
{
    /**
     * @var string
     */
    protected $id;


    public function __construct()
    {
        $this->id = uniqid();
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function isId(string $id): bool
    {
        return ($id === $this->id);
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }
}