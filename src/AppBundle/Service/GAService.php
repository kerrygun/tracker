<?php

namespace AppBundle\Service;

use Psr\Log\LoggerInterface;
use TheIconic\Tracking\GoogleAnalytics\Analytics;

class GAService
{
    /**
     * @var array
     */
    protected $gaConfig;

    /**
     * @var ClickService
     */
    protected $clickService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param array           $gaConfig
     * @param ClickService    $clickService
     * @param LoggerInterface $logger
     */
    public function __construct(
        array           $gaConfig,
        ClickService    $clickService,
        LoggerInterface $logger
    ) {
        $this->gaConfig     = $gaConfig;
        $this->clickService = $clickService;
        $this->logger       = $logger;
    }


    /**
     * @param array $parameters
     */
    public function collect(array $parameters)
    {
        $this->logger->debug('Получена команда на передачу данных в Google Analytics c параметрами', $parameters);

        $parameters = array_merge($this->gaConfig, $parameters);

        if(array_key_exists('cookie_id', $parameters) and $cookieId = $parameters['cookie_id'] and $click = $this->clickService->getLastClick($cookieId, null)) {
            $this->logger->info(sprintf('Найден отслеживающий cookie "%s"', $cookieId));

            foreach($click->getParameters() as $parameter) {
                $parameters[$parameter->getName()] = $parameter->getValue();
            }
        }

        $analytics = new Analytics();

        foreach ($parameters as $key => $value) switch ($key) {
            case 'v':              $analytics->setProtocolVersion($value); break;
            case 'tid':            $analytics->setTrackingId     ($value); break;
            case 'cid':
            case 'application_id': $analytics->setClientId       ($value); break;
            case 't':              $analytics->setHitType        ($value); break;
            case 'ec':             $analytics->setEventCategory  ($value); break;
            case 'ea':             $analytics->setEventAction    ($value); break;
            case 'el':             $analytics->setEventLabel     ($value); break;
            case 'ev':             $analytics->setEventValue     ($value); break;
            case 'cookie_id':      $analytics->setEventValue     ($value ? 1 : 0); break;
            case 'cn':
            case 'utm_campaign':   $analytics->setCampaignName   ($value); break;
            case 'cs':
            case 'utm_source':     $analytics->setCampaignSource ($value); break;
            case 'cm':
            case 'utm_medium':     $analytics->setCampaignMedium ($value); break;
            case 'ck':
            case 'utm_term':       $analytics->setCampaignKeyword($value); break;
            case 'cc':
            case 'utm_content':    $analytics->setCampaignContent($value); break;
        }

        $this->logger->debug('Данные для передачи в Google Analytics', $parameters);

        $analytics->sendEvent();

        $this->logger->debug('Данные переданы');
    }
}
