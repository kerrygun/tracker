<?php

namespace AppBundle\Service;


use AppBundle\Entity\Referer;
use AppBundle\Repository\QueryReferer;
use Doctrine\ORM\EntityManagerInterface;

class RefererService
{
    /**
     * @var QueryService
     */
    protected $queryService;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var UrlService
     */
    protected $urlService;

    /**
     * @param QueryService           $queryService
     * @param EntityManagerInterface $entityManager
     * @param UrlService             $urlService
     */
    public function __construct(
        QueryService           $queryService,
        EntityManagerInterface $entityManager,
        UrlService             $urlService
    )
    {
        $this->queryService  = $queryService;
        $this->entityManager = $entityManager;
        $this->urlService    = $urlService;

    }


    /**
     * @param string $url
     *
     * @return Referer
     *
     */
    public function get(string $url)
    {
        if(! $host = parse_url($url, PHP_URL_HOST)) {
            return null;
        }

        if (! $referer = $this->queryService->execute(QueryReferer::class, array('host' => $host)))
        {
            $this->entityManager->persist($referer = (new Referer($host))->setSource($host)->setMedium('referral'));
            $this->entityManager->flush();
        }

        return $referer;
    }
}