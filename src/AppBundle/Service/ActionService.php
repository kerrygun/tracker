<?php

namespace AppBundle\Service;

use AppBundle\Entity\Action;
use AppBundle\Entity\Target;
use AppBundle\Repository\QueryAction;
use Doctrine\ORM\EntityManagerInterface;


class ActionService
{
    /**
     * @var QueryService
     */
    protected $queryService;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param QueryService $queryService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        QueryService           $queryService,
        EntityManagerInterface $entityManager
    )
    {
        $this->queryService  = $queryService;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Target $target
     * @param string $name
     *
     * @return Action
     */
    public function getAction(Target $target, string $name)
    {
        $params = array(
            'target' => $target->getId(),
            'name'   => $name
        );

        if (! $action = $this->queryService->execute(QueryAction::class, $params))
        {
            $this->entityManager->persist($action = new Action($target, $name));
            $this->entityManager->flush();
        }

        return $action;
    }
}