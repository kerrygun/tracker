<?php

namespace AppBundle\Service;

use AppBundle\Entity\Agency;
use AppBundle\Repository\QueryAgency;
use Doctrine\ORM\EntityManagerInterface;


class AgencyService
{
    /**
     * @var QueryService
     */
    protected $queryService;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param QueryService $queryService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        QueryService           $queryService,
        EntityManagerInterface $entityManager
    )
    {
        $this->queryService  = $queryService;
        $this->entityManager = $entityManager;
    }


    /**
     * @param string $utmSource
     *
     * @return Agency
     */
    public function getAgency(string $utmSource)
    {
        if (! $agency = $this->queryService->execute(QueryAgency::class, array('source' => $utmSource)))
        {
            $this->entityManager->persist($agency = new Agency($utmSource));
            $this->entityManager->flush();
        }

        return $agency;
    }
}