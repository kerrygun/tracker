<?php

namespace AppBundle\Service;

use AppBundle\Repository\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;


class QueryService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @param string $className
     * @param array  $params
     *
     * @return mixed
     */
    public function execute($className, array $params = [])
    {
        $query = $this->createQuery($className);

        return $query->execute($params);
    }

    /**
     * @param string $className
     *
     * @return AbstractQuery
     */
    public function createQuery($className)
    {
        $reflectedClass = new ReflectionClass($className);

        return $reflectedClass->newInstanceArgs([$this->entityManager]);
    }
}
