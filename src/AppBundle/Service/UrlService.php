<?php

namespace AppBundle\Service;


class UrlService
{
    /**
     * @param string $url
     * @param array  $parameters
     *
     * @return string
     */
    public function enrichUrl(string $url, array $parameters)
    {
        $parsedUrl = parse_url($url);
        $query = [];

        if (!empty($parsedUrl['query'])) {
            parse_str($parsedUrl['query'], $query);
        }

        $parsedUrl['query'] = http_build_query(array_merge($query, $parameters));

        return http_build_url($parsedUrl);
    }

    /**
     * @param string $url
     * @param array $parameters
     *
     * @return mixed
     */
    public function replaceParameters(string $url, array $parameters)
    {
        return str_replace(
            array_map(
                function($key){return sprintf('{%s}', $key);},
                array_keys($parameters)
            ),
            array_values($parameters),
            $url
        );
    }
}