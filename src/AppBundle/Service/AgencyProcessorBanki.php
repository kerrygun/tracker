<?php

namespace AppBundle\Service;


class AgencyProcessorBanki extends AgencyProcessor
{
    /**
     * @var string
     */
    protected $source = 'banki.ru';

	/**
	 * @inheritdoc
	 */
	public function isProcessable(string $status, string $reason): bool
	{
        $reasons = ['CANCELED_CLIENT', 'REJECTED_ON_SIGN'];

		return ! in_array($reason, $reasons);
	}
}
