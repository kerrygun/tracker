<?php

namespace AppBundle\Service;


class AgencyProcessor implements AgencyProcessorInterface
{
    /**
     * @var string
     */
    protected $source;

	/**
	 * @inheritdoc
	 */
	public function getSource(): string
	{
	    return $this->source;
	}

	/**
	 * @inheritdoc
	 */
	public function getParams(array $source): array
	{
	    return $source;
	}

    /**
     * @inheritdoc
     */
	public function isProcessable(string $status, string $reason): bool
    {
        return true;
    }
}
