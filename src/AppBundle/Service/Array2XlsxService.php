<?php

namespace AppBundle\Service;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Exception;
use RuntimeException;

class Array2XlsxService
{
    /**
     * @param array  $array
     * @param string $file
     *
     * @return string
     */
    public function save(array $array, string $file)
    {
        try {
            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);

            $activeSheet = $spreadsheet->getActiveSheet();
            $activeSheet->fromArray($array);


            $writer = new Xlsx($spreadsheet);
            $writer->save($file);
        } catch (Exception $e) {
            throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
        }

        return $file;
    }
}