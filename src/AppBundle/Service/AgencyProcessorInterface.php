<?php

namespace AppBundle\Service;


interface AgencyProcessorInterface
{
	/**
	 * Возвращает source Агенства
	 *
	 * @return string
	 */
	public function getSource(): string;

	/**
	 * Возвращает новые параметры
	 *
	 * @param array $source
	 *
	 * @return array
	 */
	public function getParams(array $source): array;

    /**
     * Нужно ли процессить
     *
     * @param string $status
     * @param string $reason
     *
     * @return boolean
     */
    public function isProcessable(string $status, string $reason): bool;
}
