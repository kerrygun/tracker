<?php

namespace AppBundle\Dto;

use AppBundle\Entity\Agency;
use JsonSerializable;
use DateTime;


class ResponseAgency implements JsonSerializable
{
    /**
     * @var Agency
     */
    protected $agency;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @param Agency   $agency
     * @param DateTime $date
     */
    public function __construct(Agency $agency, ?DateTime $date)
    {
        $this->agency = $agency;
        $this->date   = $date ?? new DateTime();
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'title'  => $this->agency->getTitle(),
            'source' => $this->agency->getSource(),
            'offers' => call_user_func(function() {
                $offers = []; foreach ($this->agency->getOffersForDate($this->date) as $offer) $offers[] = new ResponseOffer($offer);

                return $offers;
            })
        );
    }
}