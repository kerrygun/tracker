<?php

namespace AppBundle\Dto;

use AppBundle\Entity\Offer;
use JsonSerializable;


class ResponseOffer implements JsonSerializable
{
    /**
     * @var Offer
     */
    protected $offer;

    /**
     * @param Offer $offer
     */
    public function __construct(Offer $offer)
    {
        $this->offer = $offer;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'uuid'   => $this->offer->getUuid(),
            'title'  => $this->offer->getName(),
        );
    }
}