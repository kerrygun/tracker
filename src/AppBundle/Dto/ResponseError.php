<?php

namespace AppBundle\Dto;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use JsonSerializable;


class ResponseError implements JsonSerializable
{
    /**
     * @var ConstraintViolationListInterface[]
     */
    protected $errors;

    /**
     * @param ConstraintViolationListInterface $errors
     */
    public function __construct(ConstraintViolationListInterface $errors)
    {
        foreach ($errors as $error) $this->errors[] = array(
            /** @var ConstraintViolationInterface $error */

            'property_path' => $error->getPropertyPath(),
            'error_code'    => $error->getCode(),
            'message'       => $error->getMessage(),
        );
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return array(
            'errors' => $this->errors,
        );
    }
}
