<?php

namespace AppBundle\Controller;

use AppBundle\Service\ClickService;
use AppBundle\Dto\ResponseAgency;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @Route(service="app.agency_controller")
 */
class AgencyController extends Controller
{
    /**
     * @var ClickService
     */
    protected $clickService;


    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param ClickService    $clickService
     * @param LoggerInterface $logger
     */
    public function __construct(
        ClickService    $clickService,
        LoggerInterface $logger
    ) {
        $this->clickService    = $clickService;
        $this->logger          = $logger;
    }

    /**
     * @param Request $request
     *
     * @Route("/agency", name="agency")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function getAction(Request $request)
    {
        if (! $cookie = $request->get('cookie'))
            throw new BadRequestHttpException('Cookie not provided');

        if (! $click = $this->clickService->getLastPaidClick($cookie, null))
            throw new NotFoundHttpException('Click not found');

        if (! $agency = $click->getAgency())
            throw new NotFoundHttpException('Agency not found');

        return new JsonResponse(new ResponseAgency($agency, null));
    }
}