<?php

namespace AppBundle\Controller;

use AppBundle\Exception\ActionException;
use AppBundle\Service\ClickService;
use AppBundle\Service\PostbackService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use DateTime;


/**
 * @Route(service="app.postback_controller")
 */
class PostbackController extends Controller
{
    /**
     * @var PostbackService
     */
    protected $postbackService;

    /**
     * @var ClickService
     */
    protected $clickService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param PostbackService $postbackService
     * @param ClickService    $clickService
     * @param LoggerInterface $logger
     */
    public function __construct(
        PostbackService $postbackService,
        ClickService    $clickService,
        LoggerInterface $logger
    )
    {
        $this->postbackService = $postbackService;
        $this->clickService    = $clickService;
        $this->logger          = $logger;
    }

    /**
     * @param Request $request
     *
     * @Route("/postback", name="postback")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function postbackAction(Request $request)
    {
        $this->logger->debug('Получена команда на postback запрос c параметрами', $request->query->all());

        $cookieId = $request->query->get('cookie');

        $till = $request->query->get('till') ? (new DateTime())->setTimestamp($request->query->get('till')) : null;

        if (! $click = $this->clickService->getLastPaidClick($cookieId, $till)) {
            $this->logger->info(sprintf('Не найден отслеживающий cookie "%s"', $cookieId));
            return new Response('Cookie не найден');
        }

        $this->logger->info(sprintf('Найден отслеживающий cookie "%s"', $cookieId));

        try {
            $this->postbackService->makePostback($click, $request->query->all());
        } catch (ActionException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), null, 400);
        }

        return new Response();
    }

    /**
     * @Route("/mock", name="mock")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function posbackSelfAction()
    {
        $this->logger->debug('Posback принят в заглушку');

        return new Response();
    }
}