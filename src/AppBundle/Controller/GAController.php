<?php

namespace AppBundle\Controller;

use AppBundle\Service\ClickService;
use AppBundle\Service\GAService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * @Route(service="app.ga_controller")
 */
class GAController extends Controller
{
    /**
     * @var GaService
     */
    protected $gaService;

    /**
     * @var ClickService
     */
    protected $clickService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param GaService       $gaService
     * @param ClickService    $clickService
     * @param LoggerInterface $logger
     */
    public function __construct(
        GaService       $gaService,
        ClickService    $clickService,
        LoggerInterface $logger
    ) {
        $this->gaService       = $gaService;
        $this->clickService    = $clickService;
        $this->logger          = $logger;
    }

    /**
     * @param Request $request
     *
     * @Route("/collect", name="collect")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function collectAction(Request $request)
    {
        $this->gaService->collect($request->query->all());

        return new Response();
    }
}