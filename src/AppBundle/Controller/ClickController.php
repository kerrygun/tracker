<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Referer;
use AppBundle\Entity\Target;
use AppBundle\Service\AgencyService;
use AppBundle\Service\ClickService;
use AppBundle\Service\RefererService;
use AppBundle\Service\TargetService;
use AppBundle\Service\UrlService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ramsey\Uuid\Uuid;
use DateTime;

use function GuzzleHttp\Psr7\parse_query;


/**
 * @Route(service="app.click_controller")
 */
class ClickController extends Controller
{
    /**
     * @var string
     */
    protected $cookieName;

    /**
     * @var integer
     */
    protected $cookieLifetime;

    /**
     * @var ClickService
     */
    protected $clickService;

    /**
     * @var TargetService
     */
    protected $targetService;

    /**
     * @var AgencyService
     */
    protected $agencyService;

    /**
     * @var RefererService
     */
    protected $refererService;

    /**
     * @var UrlService
     */
    protected $urlService;

    /**
     * @param string          $cookieName
     * @param integer         $cookieLifetime
     * @param ClickService    $clickService
     * @param TargetService   $targetService
     * @param AgencyService   $agencyService
     * @param RefererService  $refererService
     * @param UrlService      $urlService
     */
    public function __construct(
        string          $cookieName,
        int             $cookieLifetime,
        ClickService    $clickService,
        TargetService   $targetService,
        AgencyService   $agencyService,
        RefererService  $refererService,
        UrlService      $urlService
    )
    {
        $this->cookieName      = $cookieName;
        $this->cookieLifetime  = $cookieLifetime;
        $this->clickService    = $clickService;
        $this->targetService   = $targetService;
        $this->agencyService   = $agencyService;
        $this->refererService  = $refererService;
        $this->urlService      = $urlService;
    }


    /**
     * @param Request $request
     *
     * @Route("/a", name="a")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function aAction(Request $request)
    {
        $agency = ($utmSource = $request->query->get('utm_source'))
            ? $this->agencyService->getAgency($utmSource)
            : null
        ;

	    $cookieId = $request->cookies->get($this->cookieName) ? : Uuid::uuid4();

        $click = $this->clickService->createClick(
            $cookieId,
            $agency,
            $request->query->all(),
            $request->headers->get('user-agent', null, true),
            $request->headers->get('referer', null, true)
        );

        $redirect = $this->redirect($this->targetService->getUrl($click->getTarget(), array_merge($request->query->all(), array('bcs_click' => $click->getId()))), 303);
	    $redirect->headers->setCookie(new Cookie($this->cookieName, $cookieId, new DateTime(sprintf('+%s second', $this->cookieLifetime))));

	    return $redirect;
    }

    /**
     * @param Request $request
     *
     * @Route("/direct", name="direct")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function directAction(Request $request)
    {
        // если не указан целевой урл, значит это залетный переход - отправляем его обычным маршрутом
        if (! $targetUrl = $request->query->get('target')) {
            return $this->aAction($request);
        }

        $target   = new Target($targetUrl);
        $redirect = $this->redirect($target->getUrl(), 303);
        $params   = array();

        // если можем определить источник трафика, то тэгируем его
        if ($refererUrl = $request->query->get('referer') or $refererUrl = $request->headers->get('referer')) {
            if ($referer = $this->refererService->get($refererUrl)) {
                $params['utm_source'] = $referer->getSource() ?: Referer::DEFAULT_SOURCE;
                $params['utm_medium'] = $referer->getMedium() ?: Referer::DEFAULT_MEDIUM;
            }
        } else {
            $params['utm_source'] = Referer::DEFAULT_SOURCE;
            $params['utm_medium'] = Referer::DEFAULT_MEDIUM;
        }

        $targetUrl = $this->urlService->enrichUrl($target->getUrl(), $params);


        $cookieId = $request->cookies->get($this->cookieName) ? : Uuid::uuid4();

        $click = $this->clickService->createClick(
            $cookieId,
            null,
            array_filter(parse_query(parse_url($targetUrl, PHP_URL_QUERY))),
            $request->headers->get('user-agent', null, true),
            $refererUrl
        );

        $redirect->setTargetUrl($this->urlService->enrichUrl($targetUrl, array('bcs_click' => $click->getId())));
        $redirect->headers->setCookie(new Cookie($this->cookieName, $cookieId, new DateTime(sprintf('+%s second', $this->cookieLifetime))));

        return $redirect;
    }
}
