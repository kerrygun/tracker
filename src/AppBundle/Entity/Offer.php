<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;


/**
 * @ORM\Entity
 * @ORM\Table(name="tr_offer")
 */
class Offer
{
    use Identifiable;
    use Stringable;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=36, unique=true)
     */
    protected $uuid;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $isActive;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4095)
     */
    protected $description;

    /**
     *
     */
    public function __construct()
    {
        $this->uuid = Uuid::uuid4();
    }


    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @inheritdoc
     */
    public function toString()
    {
        return vsprintf('%s:{uuid=`%s`,name=`%s`}', [
            static::class,

            $this->uuid,
            $this->name,
        ]);
    }
}
