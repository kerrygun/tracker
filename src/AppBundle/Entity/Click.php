<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use DateTime;


/**
 * @ORM\Entity
 * @ORM\Table(name="tr_click")
 */
class Click
{
    use Identifiable;
    use TimestampableEntity;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Agency", inversedBy="clicks")
     * @ORM\JoinColumn(name="id_agency", referencedColumnName="id", nullable=true)
     */
    protected $agency;

    /**
     * @var Target
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Target", inversedBy="clicks")
     * @ORM\JoinColumn(name="id_target", referencedColumnName="id", nullable=false)
     */
    protected $target;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $clickId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $cookieId;

    /**
     * @var Collection|Parameter[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Parameter", mappedBy="click", fetch="EAGER")
     */
    protected $parameters;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    protected $referrer;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    protected $userAgent;

    /**
     * @param string $cookieId
     */
    public function __construct(string $cookieId)
    {
	    $this->setCreatedAt(new DateTime());
	    $this->parameters = new ArrayCollection();
        $this->cookieId   = $cookieId;
    }


    /**
     * @param Agency $agency
     *
     * @return $this
     */
    public function setAgency(Agency $agency)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param string $referrer
     *
     * @return $this
     */
    public function setReferrer(?string $referrer)
    {
        $this->referrer = $referrer;

        return $this;
    }

    /**
     * @param string $userAgent
     *
     * @return $this
     */
    public function setUserAgent(?string $userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * @param string $clickId
     *
     * @return  $this
     */
    public function setClickId(string $clickId)
    {
        $this->clickId = $clickId;

        return $this;
    }

    /**
     * @return string
     */
    public function getClickId()
    {
        return $this->clickId;
    }

    /**
     * @param Target $target
     *
     * @return $this
     */
    public function setTarget(Target $target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * @return Target
     */
    public function getTarget(): Target
    {
        return $this->target;
    }

    /**
     * @param Parameter $parameter
     *
     * @return $this
     */
    public function addParameter(Parameter $parameter)
    {
        $this->parameters->add($parameter);

        return $this;
    }

    /**
     * @return Collection|Parameter[]
     */
    public function getParameters()
    {
        return $this->parameters;
    }

	/**
	 * @return array
	 */
	public function getParamsMap()
	{
		$map = array();
		foreach ($this->parameters as $parameter)
			$map[$parameter->getName()] = $parameter->getValue();

		return $map;
    }

    /**
     * @param string $name
     *
     * @return Parameter|null
     */
    public function getParameter(string $name)
    {
        foreach ($this->getParameters() as $parameter) {
            if ($parameter->getName() === $name) {
                return $parameter;
            }
        }

        return null;
    }
}