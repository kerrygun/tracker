<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="tr_action")
 */
class Action
{
    use Identifiable;

    /**
     * @var Target
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Target", inversedBy="actions")
     * @ORM\JoinColumn(name="id_target", referencedColumnName="id", nullable=false)
     */
    protected $target;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @param Target $target
     * @param string $name
     */
    public function __construct(Target $target, string $name)
    {
        $this->target = $target;
        $this->name   = $name;
    }

    /**
     * @return Target
     */
    public function getTarget(): Target
    {
        return $this->target;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}