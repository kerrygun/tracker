<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="tr_agency_offer")
 */
class AgencyOffer
{
    use Identifiable;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Agency", inversedBy="agency_offer")
     */
    protected $agency;

    /**
     * @var Offer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Offer", inversedBy="agency_offer")
     */
    protected $offer;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="date")
     */
    protected $startAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="date")
     */
    protected $stopAt;

    /**
     * @return Offer
     */
    public function getOffer(): Offer
    {
        return $this->offer;
    }

    /**
     * @return DateTime
     */
    public function getStartAt(): DateTime
    {
        return $this->startAt;
    }

    /**
     * @return DateTime
     */
    public function getStopAt(): DateTime
    {
        return $this->stopAt;
    }
}
