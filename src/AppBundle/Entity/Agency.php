<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="tr_agency")
 */
class Agency
{
    use Identifiable;
    use TimestampableEntity;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     */
    protected $utmSource;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * Название query-параметра в адресе клика, которым определяется целевой адрес для переадресации
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $targetKey;

    /**
     * Название query-параметра в адресе клика, которым определяется id клика в системе источника трафика
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $idKey;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    protected $postBackUrl;

    /**
     * @var Action
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Action")
     * @ORM\JoinColumn(name="id_action", referencedColumnName="id", nullable=true)
     */
    protected $postbackAction;

    /**
     * @var Collection|AgencyOffer[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AgencyOffer", mappedBy="agency")
     */
    protected $agencyOffers;


    /**
     * @param string $utmSource
     */
    public function __construct(string $utmSource)
    {
        $this->utmSource    = $utmSource;
    }

    /**
     * @return Collection|AgencyOffer[]
     */
    public function getAgencyOffers()
    {
        return $this->agencyOffers ?? new ArrayCollection();
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffersForDate(DateTime $dateTime)
    {


        return
            $this->getAgencyOffers()
            ? $this->getAgencyOffers()
                ->filter(function(AgencyOffer $agencyOffer) use ($dateTime) {return ($dateTime >= $agencyOffer->getStartAt() and $dateTime < $agencyOffer->getStopAt());})
                ->map(function(AgencyOffer $agencyOffer) {return $agencyOffer->getOffer();})
            : [];
    }

    /**
     * @return Action
     */
    public function getPostbackAction()
    {
        return $this->postbackAction;
    }

    /**
     * @return string
     */
    public function getTargetKey()
    {
        return $this->targetKey;
    }

    /**
     * @return string
     */
    public function getPostBackUrl()
    {
        return $this->postBackUrl;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->utmSource;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getTitle() ?: $this->getSource();
    }

    /**
     * @return string
     */
    public function getIdKey()
    {
        return $this->idKey;
    }
}