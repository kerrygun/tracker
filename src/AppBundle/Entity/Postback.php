<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use DateTime;


/**
 * @ORM\Entity
 * @ORM\Table(name="tr_postback")
 */
class Postback
{
    use Identifiable;
    use TimestampableEntity;

    /**
     * @var Click
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Click")
     * @ORM\JoinColumn(name="id_click", referencedColumnName="id", nullable=false)
     */
    protected $click;

    /**
     * @var Action
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Action")
     * @ORM\JoinColumn(name="id_action", referencedColumnName="id", nullable=false)
     */
    protected $action;

    /**
     * @var string
     *
     * @ORM\Column(name="id_application", type="string", length=40, nullable=true)
     */
    protected $applicationId;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=4000, nullable=true)
     */
    protected $address;

    /**
     * @var \DateTime

     * @ORM\Column(type="datetime")
     */
    protected $completedAt;

    /**
     * @param Click  $click
     * @param Action $action
     * @param string $applicationId
     * @param string $address
     */
    public function __construct(Click $click, Action $action, string $applicationId, string $address)
    {
        $this->createdAt     = new DateTime();
        $this->click         = $click;
        $this->action        = $action;
        $this->applicationId = $applicationId;
        $this->address       = $address;
    }

    /**
     * @return string
     */
    public function getApplicationId()
    {
        return $this->applicationId;
    }

    /**
     * @return Click
     */
    public function getClick(): Click
    {
        return $this->click;
    }

    /**
     * @return $this
     */
    public function setCompleted()
    {
        $this->completedAt = new DateTime();

        return $this;
    }
}