<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="tr_referer")
 */
class Referer
{
    use Identifiable;

    const DEFAULT_SOURCE = '(direct)';

    const DEFAULT_MEDIUM = '(none)';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     */
    protected $host;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $source;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $medium;

    /**
     * @param string $host
     */
    public function __construct(string $host)
    {
        $this->host = $host;
    }


    /**
     * @param string $host
     *
     * @return  $this
     */
    public function setHost(string $host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @param string $source
     *
     * @return  $this
     */
    public function setSource(string $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $medium
     *
     * @return  $this
     */
    public function setMedium(string $medium)
    {
        $this->medium = $medium;

        return $this;
    }

    /**
     * @return string
     */
    public function getMedium()
    {
        return $this->medium;
    }
}