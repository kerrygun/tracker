<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="tr_parameter")
 */
class Parameter
{
    use Identifiable;

    const MEDIUM = 'utm_medium';

    const MEDIUM_ORGANIC = 'organic';
    const MEDIUM_NONE    = '(none)';


    /**
     * @var Click
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Click", inversedBy="parameters")
     * @ORM\JoinColumn(name="id_click", referencedColumnName="id", nullable=false)
     */
    protected $click;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    protected $value;

    /**
     * @param Click  $click
     * @param string $name
     * @param string $value
     */
    public function __construct(Click $click, string $name, string $value)
    {
	    $this->click = $click;
        $this->name  = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}