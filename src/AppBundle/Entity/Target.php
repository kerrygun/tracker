<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * @ORM\Entity
 * @ORM\Table(name="tr_target")
 */
class Target
{
    use Identifiable;
    use TimestampableEntity;

    const DEFAULT_KEY = 'target';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4000)
     */
    protected $name;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", length=4000)
     */
    protected $isDefault;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4000)
     */
    protected $url;

    /**
     * @var ArrayCollection|Action[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Action", mappedBy="target")
     */
    protected $actions;

    /**
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }


    /**
     * @param string $url
     *
     * @return  $this
     */
    public function setUrl(string $url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}