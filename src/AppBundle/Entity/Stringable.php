<?php

namespace AppBundle\Entity;


trait Stringable
{
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * @return string
     */
    abstract public function toString();
}
