<?php

namespace AppBundle\Util;

use AppBundle\Entity\Stringable;
use InvalidArgumentException;


class Json
{
    use Stringable;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * @param string $source
     * @param bool   $silent
     *
     * @throws InvalidArgumentException
     */
    public function __construct(string $source, bool $silent = false)
    {
        $this->data = json_decode($source);

        if ((false === $silent) and (JSON_ERROR_NONE !== json_last_error())) {
            throw new InvalidArgumentException(json_last_error_msg(), json_last_error());
        }
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get(string $name, $default = null)
    {
        return $this->data->{$name} ?? $default;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->get($name);
    }


    /**
     * @return string
     */
    public function toString()
    {
        return json_encode($this->data, JSON_UNESCAPED_UNICODE);
    }
}
