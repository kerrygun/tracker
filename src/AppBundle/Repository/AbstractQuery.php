<?php

namespace AppBundle\Repository;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query as DoctrineQuery;


abstract class AbstractQuery
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return QueryBuilder
     */
    protected function createQueryBuilder()
    {
        return $this->entityManager->createQueryBuilder();
    }

    /**
     * @param string $className
     * @param string $alias
     *
     * @return QueryBuilder
     */
    protected function selectBuilder(string $className, string $alias)
    {
        return $this->entityManager->createQueryBuilder()
            ->from($className, $alias)
            ->select($alias)
            ;
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getEntityManager() {
        return $this->entityManager;
    }

    /**
     * @param string $dql
     *
     * @return DoctrineQuery
     */
    protected function query(string $dql = '')
    {
        return $this->entityManager->createQuery($dql);
    }

    /**
     * @param string $className
     *
     * @return ObjectRepository
     */
    protected function repository(string $className)
    {
        return $this->entityManager->getRepository($className);
    }

    /**
     * @param string $sql
     *
     * @return Statement
     */
    protected function prepare(string $sql) {
        return $this->entityManager->getConnection()
            ->prepare($sql);
    }

    /**
     * Внимание!
     * Платформазивисимая операция!
     * Да ещё и без эскейпинга знака процента!
     *
     * @param string $value
     *
     * @return string
     */
    protected function like(string $value)
    {
        return '%' . $value . '%';
    }

    /**
     * @param array $params
     *
     * @return mixed
     */
    abstract public function execute(array $params = []);
}
