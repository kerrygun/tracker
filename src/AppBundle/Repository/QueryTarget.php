<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Target;
use Doctrine\ORM\QueryBuilder;


class QueryTarget extends AbstractQuery
{
    /**
     * @inheritdoc
     *
     * @return Target
     */
    public function execute(array $params = [])
    {
        $builder = $this->build($params);

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array
     *
     * @return QueryBuilder
     */
    protected function build(array $params)
    {
        $builder = $this->selectBuilder(Target::class, 'target');

        $builder
            ->setMaxResults(1)
            ->orderBy('target.isDefault', 'DESC')
            ->addOrderBy('target.createdAt', 'DESC')
        ;

        if (array_key_exists('name', $params))
        {
            $builder
                ->andWhere('target.name = :name')
                ->setParameter(':name', $params['name'])
            ;
        }

        return $builder;
    }
}
