<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Action;
use Doctrine\ORM\QueryBuilder;


class QueryAction extends AbstractQuery
{
    /**
     * @inheritdoc
     *
     * @return Action
     */
    public function execute(array $params = [])
    {
        $builder = $this->build($params);

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $params
     *
     * @return QueryBuilder
     */
    protected function build(array $params)
    {
        $builder = $this->selectBuilder(Action::class, 'action');

        $builder
            ->andWhere('action.name = :name')
            ->setParameter(':name', $params['name'])
        ;

        return $builder;
    }
}
