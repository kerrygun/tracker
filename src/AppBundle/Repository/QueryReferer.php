<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Referer;
use Doctrine\ORM\QueryBuilder;


class QueryReferer extends AbstractQuery
{
    /**
     * @inheritdoc
     *
     * @return Referer
     */
    public function execute(array $params = [])
    {
        $builder = $this->build($params);

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array
     *
     * @return QueryBuilder
     */
    protected function build(array $params)
    {
        $builder = $this->selectBuilder(Referer::class, 'referer');

        $builder
            ->setMaxResults(1)
        ;

        if (! empty($params['host']))
        {
            $builder
                ->andWhere('referer.host = :host')
                ->setParameter(':host', $params['host'])
            ;
        }

        return $builder;
    }
}
