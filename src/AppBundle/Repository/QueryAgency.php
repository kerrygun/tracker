<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Agency;
use Doctrine\ORM\QueryBuilder;


class QueryAgency extends AbstractQuery
{
    /**
     * @inheritdoc
     *
     * @return Agency
     */
    public function execute(array $params = [])
    {
        $builder = $this->build($params);

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $params
     *
     * @return QueryBuilder
     */
    protected function build(array $params)
    {
        $builder = $this->selectBuilder(Agency::class, 'agency');

        $builder
            ->andWhere('agency.utmSource = :source')
            ->setParameter(':source', $params['source'])
        ;

        return $builder;
    }
}
