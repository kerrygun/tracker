<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Click;
use Doctrine\ORM\QueryBuilder;


class QueryClicks extends AbstractQuery
{
    /**
     * @inheritdoc
     *
     * @return Click[]
     */
    public function execute(array $params = [])
    {
        $builder = $this->build($params);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param array $params
     *
     * @return QueryBuilder
     */
    protected function build(array $params)
    {
        $builder = $this->selectBuilder(Click::class, 'click');

        $builder
            ->orderBy('click.createdAt', 'DESC')
        ;

        if (array_key_exists('paid', $params) and $params['paid']) {
            $builder
                ->innerJoin('click.agency', 'agency')
                ->andWhere($builder->expr()->isNotNull('agency.postBackUrl'))
            ;
        }

        if (array_key_exists('till', $params)) {
            $builder
                ->andWhere('click.createdAt <= :till')
                ->setParameter(':till', $params['till'])
            ;
        }

        if (array_key_exists('from', $params)) {
            $builder
                ->andWhere('click.createdAt >= :from')
                ->setParameter(':from', $params['from'])
            ;
        }

        if (array_key_exists('cookieId', $params)) {
            $builder
                ->andWhere('click.cookieId = :cookieId')
                ->setParameter(':cookieId', $params['cookieId'])
            ;
        }

        return $builder;
    }
}
