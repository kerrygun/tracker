CREATE TABLE tracker.tr_referer (id INT IDENTITY NOT NULL, host NVARCHAR(255) NOT NULL, source NVARCHAR(255), medium NVARCHAR(255), PRIMARY KEY (id));
CREATE UNIQUE INDEX UNIQ_86CBAE565F8A7F73 ON tracker.tr_referer (host) WHERE host IS NOT NULL;

grant select, insert, update, delete on tracker.tr_referer to tracker;