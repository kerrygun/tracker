CREATE TABLE tracker.tr_agency_offer (id INT IDENTITY NOT NULL, agency_id INT, offer_id INT, start_at DATE NOT NULL, stop_at DATE NOT NULL, PRIMARY KEY (id));
CREATE UNIQUE INDEX UNIQ_B733943ECDEADB2A ON tracker.tr_agency_offer (agency_id) WHERE agency_id IS NOT NULL;
CREATE UNIQUE INDEX UNIQ_B733943E53C674EE ON tracker.tr_agency_offer (offer_id) WHERE offer_id IS NOT NULL;

CREATE TABLE tracker.tr_offer (id INT IDENTITY NOT NULL, uuid VARCHAR(36) NOT NULL, is_active BIT NOT NULL, name NVARCHAR(255) NOT NULL, description VARCHAR(MAX) NOT NULL, PRIMARY KEY (id));

ALTER TABLE tracker.tr_postback ADD address NVARCHAR(4000)
ALTER TABLE tracker.tr_postback ADD completed_at DATETIME2(6)